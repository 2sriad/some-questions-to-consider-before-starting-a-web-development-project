# Some questions to consider before starting a web development project:
Credits: This list includes input from multiple former co-workers and workplaces.  
All contributions are welcome!  

## Business requirements:
* [ ] What needs to be done
* [ ] What is the MVP (Minimum Viable Product)
* [ ] Will it replace an existing system or is it a new system
    * [ ] If an existing system, how many users use it and how will they be impacted
* [ ] What features can be delivered in later phases
* [ ] What is the definition of success
    * [ ] How can success be measured.
* [ ] Is there an SLA for the service
* [ ] Is there a pre-existing deadline 
* [ ] Are there intermediate milestones
* [ ] Who are the stakeholders and what are their needs
* [ ] How long does the service need to be kept running
    * [ ] When should we plan to shut it down or rewrite it.
* [ ] What are the top five business risks and how can they be mitigated
* [ ] Are there any legal requirements such as:
    * [ ] Accessibility (Section 503)
    * [ ] GDPR, UK GDPR, CCPA
    * [ ] Verifying user age / obtaining parental consent
    * [ ] Safeguarding PII (Personally Identifiable Information)
        * [ ] Is any PII data being logged
        * [ ] Does data need to be encrypted at rest (in a database)
        * [ ] Is data encrypted in transit
        * [ ] Is data backed up? Who has access to the data?
        * [ ] Is production data copied to non-prod environments (such as staging, qa) to seed it?
        * [ ] Is production data copied to local dev environments for development?
        * [ ] Does data need to be copied from production into another environment to aid with debugging issues? How is the data safeguarded?        
    * [ ] Data retention
    * [ ] Logging
    * [ ] Do these requirements need to be vetted in every PR review?
    * [ ] How often is compliance to these requirements audited? Is the audit manual or can it be automated?
* [ ] How is the system anticipated to grow
    * [ ] Number of users
    * [ ] Traffic
    * [ ] Disk space
* [ ] What is in scope and what is out of scope
* [ ] Are there similar products/services that solve the business problem (or come close) 
    * [ ] What business (and technical) lessons can be learnt from them
    * [ ] What are the pros and cons of paying for an existing product/service that solves the problem as opposed to creating a new one
* [ ] What business KPIs need to be tracked, in what format should they be delivered and who will act upon them
* [ ] Is the work effort to build a library (as opposed to a product or service).
    * [ ] If a library, who are the early adopters for integration.
* [ ] Will new features be A/B tested at launch to compare against prior versions

## User Interface Requirements:
* [ ] What tasks is the user trying to achieve and how does the UI facilitate them
* [ ] Mockups of UI interfaces

## Technical requirements:
* [ ] Consider creating a design document with answers to questions and having it peer reviewed
* [ ] When should the project be sunset (because it is no longer needed) or rewritten (because the skills needed to maintain it are no longer likely to be available)
    * [ ] Consider adding the task to sunset or rewrite the project to the roadmap of the appropriate year/quarter/month
    * [ ] What skills and capacity is needed for the continued maintenance and support of the project after launch
* [ ] Performance requirements
* [ ] Scalability requirements
    * [ ] Any anticipated traffic peaks
* [ ] How does the data need to be treated
    * [ ] Who has access to it
    * [ ] Does it need to be encrypted and if so, at what stage of the service
* [ ] Internationalization
    * [ ] Timezones
    * [ ] Currencies
    * [ ] Encoding scheme (such as UTF-8)
* [ ] Will a data migration from a legacy system to the new system be required

## Architecture:
* [ ] How many independent designs need to be evaluated and compared before embarking on the effort
* [ ] Data lifecycle: How is the data ingested, processed, stored, archived, made available, made redundant, safeguarded (encryption, access control), syndicated, exported to other systems (such as for reporting)
    * [ ] Consider creating a data flow diagram
    * [ ] Consider documenting the schema of the data
* [ ] What are the runtime components of the system that perform computation: 
    * [ ] Services responding to requests
    * [ ] Daemon processes
    * [ ] Geographic distribution of computational infrastructure: Will it reside in different parts of the world
    * [ ] Partition tolerance: CAP theorem
    * [ ] What computation occurs at each layer: Frontend, CDN, load balancer, origin, backend
    * [ ] Architecture of the backend: Microservices/SOA/Serverless computing: View generation tier, business logic tier, data access layer, third party services 
* [ ] Dependencies
    * [ ] What additional services (internal and third party) will the service depend upon.
    * [ ] Do the dependencies affect reliability, scalability, performance/throughput
* [ ] Performance:
    * [ ] What are the expected bottlenecks and how will they be overcome
* [ ] Scalability:
    * [ ] What are the expected bottlenecks and how will they be overcome
    * [ ] Will there be a need to be dynamically scalable and how will this be achieved
    * [ ] Will there be any caching and what are the implications of any data staleness
* [ ] Security:
    * [ ] What are the data security requirements
    * [ ] What is the authentication and authorization scheme
    * [ ] What are possible attack vectors
    * [ ] How frequently should a security audit be conducted and who should conduct it
    * [ ] How will a potential data breach be addressed (customer communication, legal implications, PR)
* [ ] Availability
    * [ ] What are the availability goals and how does the design attain those goals
    * [ ] What is the failover strategy: What can fail at each layer of the system and how will it be mitigated
    * [ ] How will the system react to different types of failures in infrastructure or services that it depends on (third party and internal)
        * [ ] Infrastructure or dependency exhibits intermittent availability or high response times
        * [ ] Infrastructure or dependency exhibits Runtime errors
        * [ ] Infrastructure or dependency performs throttling
        * [ ] Infrastructure or dependency experiences complete failure
* [ ] Cost: What is the current and predicted future cost of the system based on the expected throughput and rate of growth.
* [ ] Logging
    * [ ] What should be logged and at what level.
    * [ ] What is the log retention policy
    * [ ] Will a request ID be passed through every layer of the system and logged for tracing a request while debugging
* [ ] Testing: Are any of the following are relevant and how will they be achieved on a one time / ongoing basis:
    * [ ] Stress tests (scalability)
    * [ ] Performance tests (throughput/response time)
    * [ ] A/B tests
    * [ ] Unit and integration tests
    * [ ] Testing systems that integrate with us and depend on us
    * [ ] UI tests
    * [ ] Is it possible and useful to test the latest code in dev against prod data
    * [ ] Chaos Monkey based resiliency testing
    * [ ] Testing with early adopters
    * [ ] How will test data be managed and when important be safeguarded from accidentally leaking to production
    * [ ] Manual tests
* [ ] Build and deployment process:
    * [ ] How many environments are needed (local, dev, ref, train, stage, qa, production)
    * [ ] Can everything be developed and tested locally. How quickly can a change be compile and tested
    * [ ] Consider creating a proof of concept. What are the goals of the proof of concept
    * [ ] What is the code review process: How many approvals are needed before code can be merged?
    * [ ] CI/CD
        * [ ] Will new features be hidden behind feature flags
        * [ ] Can builds be deployed to each environment (including production) in an automated fashion if automated tests pass or is a manual approval step needed before deploying to production
    * [ ] Will there be a blue-green deployment
    * [ ] How will failed deployments be rolled back. Will the rollback be automatic
* [ ] Development methodology: Waterfall/Scrum/Kanban
* [ ] Operations:
    * [ ] How will the system be monitored. What events will we react to and how. 
    * [ ] What is normal and what is a problem
    * [ ] How long should the monitoring system wait before alerting someone
    * [ ] Can any steps to remediate the issue be automated to form a self healing system
    * [ ] Can system parameters be modified dynamically at runtime
    * [ ] Is there documentation that describes how to handle the problem
        * [ ] Consider creating operational support runbook for the support team/oncall
    * [ ] What technical metrics need to be tracked, who will track them and who will act upon them
    * [ ] How will customer service requests make their way to the dev team and how will they be prioritized and addressed
    * [ ] What is the oncall rotation for supporting the service
    * [ ] Third party dependencies: 
        * [ ] What are their SLAs
        * [ ] How do we monitor them
        * [ ] How can they be contacted for support
    * [ ] What are the top five most likely operational issues and how will they be addressed
    * [ ] What regular operational tasks will need to be performed and how can they be automated
    * [ ] When will the operations/support/oncall team be trained on the new product/service to be able to support it.

## Dev Team:
* [ ] What additional training do dev team members need in order to be able to complete the project and when can that be scheduled
* [ ] How will a new engineer joining the team be trained (documentation/ 1-1 training)

## Project Management:
* [ ] Does this project depend on other projects/teams
    * [ ] How much lead time do other teams that this project depends upon need to fulfill requests
    * [ ] How much time is needed during development/testing to collaborate with third parties for integration testing 
* [ ] Estimate: What is a rough estimate for the project
* [ ] High level project plan: Consider creating a high level project plan with intermediate milestones. What is the critical path. What buffer is available. What can go wrong and how will it be addressed.

## Launch Strategy:
* [ ] Customer communication strategy: Will we notify customers in advance. How far in advance?
* [ ] Who needs to be present to support the launch (consult vacation schedules)
* [ ] How long after launch should the service be actively monitored by the launch team
* [ ] Launch checklist
    * [ ] Has it been deployed to prod
    * [ ] A/B tests if any activated and validated
    * [ ] Logging functionality validated
    * [ ] Performance monitoring and metrics at each layer of the stack verified
    * [ ] Usage metrics verified
    * [ ] Customer feedback channels tested and verified
    * [ ] Product owner sign-off
    * [ ] How will the launch of the project be celebrated

## Post launch:
* [ ] Have A/B tests been deactivated
* [ ] Gather and analyze business KPIs/metrics on whether project goals were met
* [ ] What immediate customer feedback needs to be addressed or added to the backlog
* [ ] Any new technologies/ideas/patterns/lessons learnt that can be shared
* [ ] Will additional team capacity need to be scheduled/allocated for post deployment support
* [ ] Any process documentation that needs to be updated


## Change management checklist:
* [ ] Will the proposed change have any impact on functionality or performance
* [ ] How will the list of all deployments/changes be documented
* [ ] What documentation needs to be updated to reflect enhanced features being deployed
* [ ] Who needs to approve the change
* [ ] When will the change be deployed
* [ ] What is the rollback plan
* [ ] Have team members supporting the service been trained to support the proposed features being deployed
